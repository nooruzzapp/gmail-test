from __future__ import print_function
import httplib2
import base64
import os
from oauth2client import file
from googleapiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools
from time import sleep
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import mimetypes
from email.mime.text import MIMEText
from googleapiclient import errors
try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/drive-python-quickstart.json
#It was already present
SCOPES = [  'https://www.googleapis.com/auth/drive.metadata.readonly',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/gmail.readonly',
            'https://www.googleapis.com/auth/gmail.send', #Needed to send message
            'https://mail.google.com/'
        ]

#SCOPES = 'https://www.googleapis.com/auth/drive.metadata.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Drive API Python Quickstart'


def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'drive-python-quickstart.json')
    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

'''
    To send gmail message
'''
def send_message(service, user_id, message):
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        print ('Message Id: %s' % message['id'])
        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)

'''
  To create the gmail message
'''
def create_message(sender, to, subject, message_text):
    message = MIMEText(message_text)
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    return {'raw': base64.urlsafe_b64encode(message.as_string())}

'''
    Main
'''
def main():
    credentials = get_credentials()
    sender = 'me'
    to = 'vinaykumar1989@gmail.com'
    subject = 'Test message'
    message_text = 'this works'
    
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)
    userInfoService = discovery.build('oauth2', 'v2', http=http)
    gmailService = discovery.build('gmail', 'v1', http=http)
    
    
    message = create_message( sender, to, subject, message_text)
    
    send_message(gmailService, "me", message)    
    
    gmailResult = gmailService.users().labels().list(userId='me').execute()
    labels = gmailResult.get('labels', [])
    if not labels:
        print('No labels found.')
    else:
        print('Labels:')
        for label in labels:
            print(label['name'])


    userInfo = userInfoService.userinfo().get().execute()
    print(userInfo['email'] + userInfo['given_name'])
    results = service.files().list(
        pageSize=50,fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            print('{0} ({1})'.format(item['name'], item['id']))

if __name__ == '__main__':
    main()
